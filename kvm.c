#include <stdio.h>
#include <linux/kvm.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <err.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>

#include "definition.h"

#define MEM_SIZE 0x10000

// Some macros for printing errors;
#define error(fmt, ... ) do {                                           \
    fprintf(stderr, fmt , ##__VA_ARGS__ );                              \
    exit(EXIT_FAILURE);                                                 \
  }while(0);

#define pexit(x) do {                           \
  perror(x);                                    \
  exit(EXIT_FAILURE);                           \
  }while(0);

struct VM {
  int kvmfd;
  int vcpufd;
  struct kvm_run * run;
};
struct kvm_guest_debug debug = {
                                .control = KVM_GUESTDBG_ENABLE | KVM_GUESTDBG_SINGLESTEP
};

void debug_(void){
  ioctl(0x5,KVM_SET_GUEST_DEBUG,&debug);
}

int main()
{
  uint8_t code[] = "H\xB8\x41\x42\x43\x44\x31\x32\x33\nj\bY\xBA\x17\x02\x00\x00\xEEH\xC1\xE8\b\xE2\xF9\xF4";

  struct VM * vm = (struct VM *) malloc(sizeof(struct VM));
  int kvm,ret;
  struct kvm_sregs sregs;
  kvm = open("/dev/kvm", O_RDWR | O_CLOEXEC);
  if(kvm < 0) pexit("open(/dev/kvm)");

  /* Check the version of the API Stable Version 12 */
  ret = ioctl(kvm, KVM_GET_API_VERSION, NULL);
  if(ret < 0 ) pexit("KVM_GET_API_VERSION");

  if(ret != KVM_API_VERSION)
    error("KVM_GET_API_VERSION %d ,expected 12",ret);

  /* Create a new VM */
  vm->kvmfd = ioctl(kvm , KVM_CREATE_VM);
  if(vm->kvmfd < 0 ) pexit("KVM_CREATE_VM");

  /* Allocate one page to hold the guest code */
  void *  mem = mmap(NULL,MEM_SIZE,
                     PROT_READ | PROT_WRITE,
                     MAP_SHARED | MAP_ANONYMOUS
                     ,-1 , 0
                     );

  if(mem==NULL) pexit("mmap(MEM_SIZE");
  memcpy(mem,code,sizeof(code));
  /* Map it to the second page frame (to avoid the real-mode IDT at 0). */
  struct kvm_userspace_memory_region region = {
                                               .slot = 0,
                                               .guest_phys_addr = 0x0,
                                               .memory_size = MEM_SIZE,
                                               .userspace_addr = (uint64_t)mem,
  };

  ret = ioctl(vm->kvmfd, KVM_SET_USER_MEMORY_REGION, &region);
  if (ret < 0 ) perror("KVM_SET_USER_MEMORY_REGION");

  /* Create a virtual CPU represents the state of one emulated CPU, including processor registers and other execution state. */

  vm->vcpufd = ioctl(vm->kvmfd , KVM_CREATE_VCPU ,(unsigned long)0);
  if(vm->vcpufd < 0 ) pexit("KVM_CREATE_VCPU");

  ret = ioctl(kvm ,KVM_GET_VCPU_MMAP_SIZE,NULL);
  if(ret < 0 ) pexit("KVM_GET_VCPU_MMAP_SIZE");

  size_t mmap_size = ret;
  vm->run = (struct kvm_run *)mmap(NULL,mmap_size ,
                                   PROT_READ | PROT_WRITE ,
                                   MAP_SHARED , vm->vcpufd , 0);
  /* Get the current special register state */
  ret = ioctl(vm->vcpufd,KVM_GET_SREGS,&sregs);
  if(ret < 0) pexit("KVM_GET_SREGS");

  sregs.cs.base = 0;
  sregs.cs.selector = 0;

  ret = ioctl(vm->vcpufd,KVM_SET_SREGS,&sregs);
  if(ret < 0 ) pexit("KVM_SET_SREGS");

  /* Initialize registers: instruction pointer for our code, addends, and
   * initial flags required by x86 architecture. */
  struct kvm_regs regs = {
                          .rip = 0x0,
                          .rax = 2,
                          .rbx = 2,
                          .rflags = 0x2,
                          .rsp = 0x4000
  };
  ret = ioctl(vm->vcpufd, KVM_SET_REGS, &regs);
  if (ret < 0 ) pexit("KVM_SET_REGS");


  /* Setting up Page table to jump to long mode */
  ret = ioctl(vm->vcpufd,KVM_GET_SREGS,&sregs);
  if(ret < 0) pexit("KVM_GET_SREGS");

  uint64_t pml4_addr = 0x1000;
  uint64_t *  pml4 = (void * )(mem + pml4_addr);

  uint64_t pdp_addr = pml4_addr + 0x1000;
  uint64_t *  pdp = (void * )(mem + pdp_addr);

  uint64_t pd_addr = pdp_addr +  0x1000;
  uint64_t *  pd = (void * )(mem + pd_addr);

  pml4[0] = PDE64_PRESENT | PDE64_RW | PDE64_USER | pdp_addr;
  pdp[0] = PDE64_PRESENT | PDE64_RW | PDE64_USER | pd_addr;
  pd[0] = PDE64_PRESENT | PDE64_RW | PDE64_PS; /* kernel only, no PED64_USER */

  sregs.cr3 = pml4_addr;
  sregs.cr4 = CR4_PAE;
  sregs.cr4 |= CR4_OSFXSR | CR4_OSXMMEXCPT; /* enable SSE instruction */
  sregs.cr0 = CR0_PE | CR0_MP | CR0_ET | CR0_NE | CR0_WP | CR0_AM | CR0_PG;
  sregs.efer = EFER_LME | EFER_LMA;
  sregs.efer |= EFER_SCE; /* enable syscall instruction */

  if(ioctl(vm->vcpufd, KVM_SET_SREGS, &sregs) < 0) pexit("ioctl(KVM_SET_SREGS)");

  if(ioctl(vm->vcpufd, KVM_GET_SREGS, &sregs) < 0) pexit("ioctl(KVM_GET_SREGS)");
  struct kvm_segment seg = {
                            .base = 0,
                            .limit = 0xffffffff,
                            .selector = 1 << 3,
                            .present = 1,
                            .type = 0xb, /* Code segment */
                            .dpl = 0, /* Kernel: level 0 */
                            .db = 0,
                            .s = 1,
                            .l = 1, /* long mode */
                            .g = 1
  };
  sregs.cs = seg;
  seg.type = 0x3; /* Data segment */
  seg.selector = 2 << 3;
  sregs.ds = sregs.es = sregs.fs = sregs.gs = sregs.ss = seg;
  if(ioctl(vm->vcpufd, KVM_SET_SREGS, &sregs) < 0) pexit("ioctl(KVM_SET_SREGS)");


  /* Repeatedly run code and handle VM exits. */
  while (1) {
    struct kvm_regs regs;
    ret = ioctl(vm->vcpufd, KVM_RUN, NULL);

    ioctl(0x5,KVM_SET_GUEST_DEBUG,&debug);
    if (ret < 0) perror("KVM_RUN");

    switch (vm->run->exit_reason)
      {

      case KVM_EXIT_HLT: puts("KVM_EXIT_HLT"); return 0;
      case KVM_EXIT_DEBUG:
        /* ioctl(vm->vcpufd,KVM_GET_REGS,&regs); */
        /*   printf("====  DEBUG ====\nRIP\t%llx\nRAX\t%llx\n",regs.rip,regs.rax); */
        break;
      case KVM_EXIT_IO:
        if (vm->run->io.direction == KVM_EXIT_IO_OUT
            && vm->run->io.size == 1
            && vm->run->io.port == 0x217
            && vm->run->io.count == 1)
            {
              putchar(*(((char *)vm->run) + vm->run->io.data_offset));
            }
          else
            perror("unhandled KVM_EXIT_IO");
          break;

        case KVM_EXIT_FAIL_ENTRY:
          error("KVM_EXIT_FAIL_ENTRY: hardware_entry_failure_reason = 0x%llx",
                (unsigned long long)vm->run->fail_entry.hardware_entry_failure_reason);

        case KVM_EXIT_INTERNAL_ERROR:
          error("KVM_EXIT_INTERNAL_ERROR: suberror = 0x%x", vm->run->internal.suberror);
        default:
          error("exit_reason = 0x%x", vm->run->exit_reason);

        }
    }
}
